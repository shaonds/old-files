This is the Project 3 of CSc 336 course Spring 2014
URL
http://www-cs.ccny.cuny.edu/~gupt20/project_3/project_3TaskIndex.php

This project's functionality is written bellow:
1)A CD has a title, a year of production and a CD type. You can come with you own CD types.  
2)A CD usually has multiple songs on different tracks. Each song has a name, an artist and a track number. Entity set Song is considered to be weak and needs support from entity set CD. 
3)A CD is produced by a producer which has a name and an address. 
4)A CD may be supplied by multiple suppliers, each has a name and an address.  
5)A customer may rent multiple CDs. Customer information such as Social Security Number (SSN), name, telephone needs to be recorded.  The date and period of renting (in days) should also be recorded. 
6)A customer may be a regular member and a VIP member. A VIP member has additional information such as the starting date of VIP status and percentage of discount. 


Attached files:

DB_connect.php--            To connect the database
project_3TaskIndex.php---Index 
project3Task1ERDiagram.doc
project3Task1ERDiagram.png   - ER Diagram
project3Task2SQLStatements.sql---Task 2 (Database relation according to the ER Diagram and                                                          SQL statements)
project_3Task2databaseRelation.txt---Database relation according to the ER Diagram
project_3createDB.php ---  Database creation
project_3insertProducer.php----Insertion  a producer 
project_3InsertingCD.php---   Insertion of  a CD supplied by a particular supplier and produced by a                                                particular producer 
project_3borrowedCD.php----Insert a regular-customer borrowing a particular CD 
project_3VIPCustomer.php----Insert a   VIP customer borrowing a particular CD 
project_3borrowedCD.php---Find names and Tel# of all customers who borrowed a particular CD                                                    and are supposed to return by a particular date. 
project_3listProducers.php---List producers who produce CD of a particular artist released in a                                              particular year


References:
cds_test.php
http://www.w3schools.com/php/

