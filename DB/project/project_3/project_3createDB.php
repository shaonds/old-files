<html>
<head><title>CD Database</title></head>
<body>
<?php
include("DB_connect.php");

	$os = "linux";
    if( strtoupper(substr(php_uname(), 0, 3)) == 'WIN'){
		$LF = "\r\n";
		$os = "win";
		echo $LF."OS Version is windows<br/><br/>".$LF;
	}
	else {
		$LF = "\n";
		echo $LF."OS Version is Unix<br/><br/>".$LF;
	}

	//drop all tables before recreate (drop relationship sets first then drop entity sets)
	mysql_query("DROP TABLE CD");
	mysql_query("DROP TABLE Producer");
	mysql_query("DROP TABLE ProducedBy");
	mysql_query("DROP TABLE Supplier");
	mysql_query("DROP TABLE SuppliedBy");
	mysql_query("DROP TABLE Songs");
	mysql_query("DROP TABLE Customers");
	mysql_query("DROP TABLE VIPCustomers");
	mysql_query("DROP TABLE Rental");
	
	//create CD table
	$sql_CD_table = "CREATE TABLE CD (Title VARCHAR(15) NOT NULL,
	                        Year_Production INT NOT NULL,
							CD_TYPE VARCHAR (30),
							Supplier_id INT NOT NULL,
					        Producer_id INT NOT NULL,
	                        PRIMARY KEY (Title, Year_Production))";  
	echo "<b>".$sql_CD_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_CD_table, $conn);
	if ( $b) {
		echo "CD table created successfully<br/><br/>".$LF;
	}

	//create Producer table
	$sql_Producer_table = "CREATE TABLE Producer (Producer_name VARCHAR(30) NOT NULL,
									Producer_address VARCHAR(30) NOT NULL,
									Producer_id INT NOT NULL AUTO_INCREMENT,
									PRIMARY KEY (Producer_name, Producer_address), INDEX (Producer_id))";
	echo "<b>".$sql_Producer_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_Producer_table, $conn);
	if ($b) {
		echo "Producer table created successfully<br/><br/>".$LF;
	}
	
	//create ProducedBy table
	$sql_ProducedBy_table = "CREATE TABLE ProducedBy (Title VARCHAR(30) NOT NULL,
					                Year_Production INT NOT NULL,
					                Producer_name VARCHAR(30) NOT NULL,
						            Producer_address VARCHAR(30) NOT NULL,
	                                PRIMARY KEY (Title, Year_Production,Producer_name,Producer_address),
					                FOREIGN KEY (Title, Year_Production)
						                         REFERENCES CD(Title, Year_Production),
						            FOREIGN KEY (Producer_name, Producer_address)
						                         REFERENCES Producer(Producer_name, Producer_address))";
	echo "<b>".$sql_ProducedBy_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_ProducedBy_table, $conn);
	if ($b) {
		echo "ProducedBy table created successfully<br/><br/>".$LF;
	}
	
	//create Supplier table
	$sql_Supplier_table = "CREATE TABLE Supplier (Supplier_name VARCHAR(30) NOT NULL,
					               Supplier_address VARCHAR(30) NOT NULL,
								   Supplier_id INT NOT NULL AUTO_INCREMENT,
					               PRIMARY KEY (Supplier_name, Supplier_address),INDEX (Supplier_id))";
	echo "<b>".$sql_Supplier_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_Supplier_table, $conn);
	if ($b) {
		echo "Supplier table created successfully<br/><br/>".$LF;
	}

	//create SuppliedBy table
	$sql_SuppliedBy_table = "CREATE TABLE SuppliedBy (Title VARCHAR(30) NOT NULL,
					                Year_Production INT NOT NULL,
					                Supplier_name VARCHAR(30) NOT NULL,
						            Supplier_address VARCHAR(30) NOT NULL,
						            PRIMARY KEY (Title, Year_Production,Supplier_name, Supplier_address ),
					                FOREIGN KEY (Title, Year_Production)
						                         REFERENCES CD(Title, Year_Production),
						            FOREIGN KEY (Supplier_name, Supplier_address)
						                         REFERENCES Supplier(Supplier_name, Supplier_address))";
	echo "<b>".$sql_SuppliedBy_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_SuppliedBy_table, $conn);
	if ($b) {
		echo "SuppliedBy table created successfully<br/><br/>".$LF;
	}

	//create Songs table
	$sql_Songs_table = "CREATE TABLE Songs(Title VARCHAR(30) NOT NULL,
					            Year_Production INT NOT NULL,
					            Song_name VARCHAR(30) NOT NULL,
					            Artist VARCHAR(30) NOT NULL,
					            Track_number INT NOT NULL,
					            PRIMARY KEY (Title, Year_Production, Song_name, Artist),
					            FOREIGN KEY (Title, Year_Production)
						                     REFERENCES CD(Title, Year_Production))";	
	echo "<b>".$sql_Songs_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_Songs_table, $conn);
	if ($b) {
		echo "Songs table created successfully<br/><br/>".$LF;
	}

	//create Customers table
	$sql_Customers_table = "CREATE TABLE Customers (SSN INT NOT NULL,
						           Customer_name VARCHAR(30) NOT NULL,
						           Telephone INT NOT NULL,
						           PRIMARY KEY (SSN))";	
	echo "<b>".$sql_Customers_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_Customers_table, $conn);
	if ($b) {
		echo "Customers table created successfully<br/><br/>".$LF;
	}

	//create VIPCustomer table
	$sql_VIPCustomer_table = "CREATE TABLE VIPCustomer(SSN INT NOT NULL,
							         Starting_date INT NOT NULL,
						             Percent_discount INT NOT NULL,
							         PRIMARY KEY (SSN),	
							         FOREIGN KEY (SSN) REFERENCES Customers(SSN),INDEX(Starting_date))";
	echo "<b>".$sql_VIPCustomer_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_VIPCustomer_table, $conn);
	if ($b) {
		echo "VIPCustomer table created successfully<br/><br/>".$LF;
	}

	//create Rental table
	$sql_Rental_table = "CREATE TABLE Rental(Title VARCHAR(30) NOT NULL,
					            Year_Production INT NOT NULL,
					            SSN INT NOT NULL,
					            Rent_Date INT NOT NULL,
					            Period INT NOT NULL,
					            Number_of_rents INT NOT NULL,
					            PRIMARY KEY (Title, Year_Production, SSN, Rent_Date, Period ),
					            FOREIGN KEY (Title, Year_Production)
						                     REFERENCES CD(Title, Year_Production),
					            FOREIGN KEY (SSN)  REFERENCES Customer(SSN), INDEX(SSN))";
	echo "<b>".$sql_Rental_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_Rental_table, $conn);
	if ($b) {
		echo "Rental table created successfully<br/><br/>".$LF;
	}
	
	mysql_close($conn);	
?>	
</body></html>