use d411;
/*Task:  2  Database relations

CD (Title, Year_production, CD_type, Supplier_id, Producer_id)
Producer (Producer_name, Producer_address,Producer_id)
Produced_by (Title, Year_production, Producer_name, Producer_address)
Supplier (Supplier_name, Supplier_address,Supplier_id)
Supplied_by (Title, Year_production, Supplier_name, Supplier_address)
Songs (Title, Year_production,  Song_name, artist, Track_number)
Customers (SSN, Customer_name, Telephone)
VIP_Customer (SSN, Starting_date, Percent_discount)
Rental (Title, Year_production, SSN, Rent_Date, period, Number_of_rents)
*/
/*Task 2 SQL Statements*/
CREATE TABLE CD ( 
					Title VARCHAR(30) NOT NULL,
					Year_Production INT NOT NULL,
					CD_TYPE VARCHAR (30),
					Supplier_id INT NOT NULL,
					Producer_id INT NOT NULL,
					PRIMARY KEY (Title, Year_Production)
				);
		
CREATE TABLE Producer ( 
					    Producer_name VARCHAR(30) NOT NULL,
					    Producer_address VARCHAR(30) NOT NULL,
						Producer_id INT NOT NULL AUTO_INCREMENT,
					    PRIMARY KEY (Producer_name, Producer_address)
				      );
					
CREATE TABLE ProducedBy ( 
					       Title VARCHAR(30) NOT NULL,
					       Year_Production INT NOT NULL,
					       Producer_name VARCHAR(30) NOT NULL,
						   Producer_address VARCHAR(30) NOT NULL,
						   PRIMARY KEY (Title, Year_Production,Producer_name,Producer_address),
					       FOREIGN KEY (Title, Year_Production)
						                REFERENCES CD(Title, Year_Production),
						   FOREIGN KEY (Producer_name, Producer_address)
						                REFERENCES Producer(Producer_name, Producer_address)
				         );
					
CREATE TABLE Supplier ( 
					    Supplier_name VARCHAR(30) NOT NULL,
					    Supplier_address VARCHAR(30) NOT NULL,
						Supplier_id INT NOT NULL AUTO_INCREMENT,
					    PRIMARY KEY (Supplier_name, Supplier_address)
				      );
					
CREATE TABLE SuppliedBy ( 
					      Title VARCHAR(30) NOT NULL,
					      Year_Production INT NOT NULL,
					      Supplier_name VARCHAR(30) NOT NULL,
						  Supplier_address VARCHAR(30) NOT NULL,
						  PRIMARY KEY (Title, Year_Production,Supplier_name, Supplier_address ),
					      FOREIGN KEY (Title, Year_Production)
						              REFERENCES CD(Title, Year_Production),
						  FOREIGN KEY (Supplier_name, Supplier_address)
						              REFERENCES Supplier(Supplier_name, Supplier_address)
				         );
					
CREATE TABLE Songs ( 
					Title VARCHAR(30) NOT NULL,
					Year_Production INT NOT NULL,
					Song_name VARCHAR(30) NOT NULL,
					Artist VARCHAR(30) NOT NULL,
					Track_number INT NOT NULL,
					PRIMARY KEY (Title, Year_Production, Song_name, Artist),
					FOREIGN KEY (Title, Year_Production)
						        REFERENCES CD(Title, Year_Production)
				   );
						 
CREATE TABLE Customers ( 
						 SSN INT NOT NULL,
						 Customer_name VARCHAR(30) NOT NULL,
						 Telephone INT NOT NULL,
						 PRIMARY KEY (SSN)
						);
						
CREATE TABLE VIPCustomer ( 
						    SSN INT NOT NULL,
							Starting_date INT NOT NULL,
						    Percent_discount INT NOT NULL,
							PRIMARY KEY SSN,	
							FOREIGN KEY (SSN) REFERENCES Customers(SSN)
						   );
						
CREATE TABLE Rental ( 
					  Title VARCHAR(30) NOT NULL,
					  Year_Production INT NOT NULL,
					  SSN INT NOT NULL,
					  Rent_Date INT NOT NULL,
					  Period INT NOT NULL,
					  Number_of_rents INT NOT NULL,
					  PRIMARY KEY (Title, Year_Production, SSN, Rent_Date, Period ),
					  FOREIGN KEY (Title, Year_Production)
						           REFERENCES CD(Title, Year_Production),
					  FOREIGN KEY (SSN)  REFERENCES Customer(SSN)
					);
					
					