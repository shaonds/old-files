<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>CD Database</title></head>
    <body>
	  <font size="24"><b>CD Database</b></font>
	  <br>
			CSC 336 Spring'14 Project 3 By Shaon Gupta
	  <br>
	    <h>To view ER Diagram</h>
         <table border="1" cellpadding="0" cellspacing="0">
 			<a href = 'project3Task1ERDiagram.png' target = '_blank' > View ER Diagram  </a>
		 </table> 
	    
		<h>To download SQL statements</h>
         <table border="1" cellpadding="0" cellspacing="0">
 			<a href = 'project3Task2SQLStatements.sql' target = '_blank' > Download SQL statements </a>
		 </table>
	    
		<h>Create tables for Tasks 2 </h>
         <table border="1" cellpadding="0" cellspacing="0">
 			<a href = 'project_3createDB.php' target = '_blank' > Create tables in database  </a>
		 </table>
		 
        <h> Task 3 to allow users to choose from the following 6 queries</h>
         <table border="1" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<a href = 'project_3insertProducer.php' target = '_blank' > Task 3.1:Insert a Producer </a>
				</td>
			</tr>
			<tr>
				<td>
					<a href = 'project_3InsertingCD.php' target = '_blank' > Task 3.2:Insert a CD supplied by a particular supplier and produced by a particular producer  </a>
				</td>
			</tr>
			<tr>
				<td>
					<a href = 'project_3regCustomer.php' target = '_blank' > Task 3.3:Insert a regular-customer borrowing a particular CD  </a>
				</td>
			</tr>
			<tr>
				<td>
					<a href = 'project_3VIPCustomer.php' target = '_blank' > Task 3.4:Insert a VIP customer borrowing a particular CD  </a>
				</td>
			</tr>
			<tr>
				<td>
					<a href = 'project_3borrowedCD.php' target = '_blank' > Task 3.5:Find names and Telephone # of all customers who borrowed a particular CD and are supposed to return by a particular date </a>
				</td>
			</tr>		
		
			<tr>
				<td>
					<a href = 'project_3listProducers.php' target = '_blank' > Task 3.6:List producers who produce CD of a particular artist released in a particular year </a>
				</td>
			</tr>			
         </table>
    </body>
</html>
