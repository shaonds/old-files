<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>MTA Database</title></head>
    <body>
	  <font size="24"><b>MTA Database</b></font>
	  <br>
			CSC 336 Spring'14 Project 2 By Shaon Gupta
	  <br>
	    <h>To view ER Diagram</h>
         <table border="1" cellpadding="0" cellspacing="0">
 			<a href = 'project_2Task1ERDiagram1.png' target = '_blank' > View ER Diagram  </a>
		 </table>
	    
		<h>Create tables for Tasks 2 and 3</h>
         <table border="1" cellpadding="0" cellspacing="0">
 			<a href = 'project_2createDB.php' target = '_blank' > Create tables in database  </a>
		 </table>
		 
		<h>Populate tables with given data</h>
         <table border="1" cellpadding="0" cellspacing="0">
 			<a href = 'project_2PopulateData.php' target = '_blank' > Load data into database  </a>
		 </table>

        <h> Task 3 to allow users to choose from the following 5 queries</h>
         <table border="1" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<a href = 'project_2Task3A.php' target = '_blank' > Task 3A: Find all control areas whose numbers of turnstiles are greater than 15 </a>
				</td>
			</tr>
			<tr>
				<td>
					<a href = 'project_2Task3B.php' target = '_blank' > Task 3B: List remote unit ids that have more than one control areas </a>
				</td>
			</tr>
			<tr>
				<td>
					<a href = 'project_2Task3C.php' target = '_blank' > Task 3C: List station names with at least one control area that has more than 15 turnstiles (SCP tuples) </a>
				</td>
			</tr>
			<tr>
				<td>
					<a href = 'project_2Task3D.php' target = '_blank' > Task 3D: List station names that Line A (A train) stops in descending order of latitude (from north to south) </a>
				</td>
			</tr>
			<tr>
				<td>
					<a href = 'project_2Task3E.php' target = '_blank' > Task 3E: Tabulate station names, ids and total numbers of control areas in the middle town area, e.g., latitude between 40.750 and 40.760 and longitude between -74.000 and -73.95 </a>
				</td>
			</tr>		
		
			<tr>
				<td>
					<a href = 'project_2Task3F.php' target = '_blank' > Task 3F: Tabulate station names, ids, total numbers of control areas and total turnstyles in the middle town area, e.g., latitude between 40.750 and 40.760 and longitude between -74.000 and -73.950 </a>
				</td>
			</tr>			
         </table>
    </body>
</html>
