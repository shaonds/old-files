
use d411;

CREATE TABLE RemoteUnits ( 
						RemoteUnit VARCHAR(15) NOT NULL,
						PRIMARY KEY (RemoteUnit)
					);
				
CREATE TABLE ControlArea (					
							ControlAreaUnitId INT NOT NULL,
							ControlAreaId VARCHAR(15) NOT NULL,
							PRIMARY KEY (ControlAreaUnitId, ControlAreaId)
						);

CREATE TABLE SubwayLines (
							LineName VARCHAR(30) NOT NULL,
							PRIMARY KEY (LineName)
						);

CREATE TABLE Station (
						StationId INT NOT NULL,
						StationName VARCHAR(50),
						Latitude FLOAT,
						Longitude FLOAT,
						PRIMARY KEY (StationId)
					);

CREATE TABLE Turnstile (
						ControlAreaUnitId INT NOT NULL,
						ControlAreaId VARCHAR(15) NOT NULL,
						SCP VARCHAR(15) NOT NULL,
						UNIQUE KEY (ControlAreaUnitId, ControlAreaId, SCP),
						FOREIGN KEY (ControlAreaUnitId, ControlAreaId) REFERENCES ControlArea(ControlAreaUnitId, ControlAreaId)
					);

CREATE TABLE Connects (
						StationId INT NOT NULL,
						LineName VARCHAR(30) NOT NULL,
						UNIQUE KEY (StationId, LineName),
						FOREIGN KEY (StationId) REFERENCES Station(StationId),
						FOREIGN KEY (LineName) REFERENCES SubwayLines(LineName)
					);	

CREATE TABLE DataCollectedBy (
								StationId INT NOT NULL,
								ControlAreaUnitId INT NOT NULL,
								ControlAreaId VARCHAR(15) NOT NULL,
								UNIQUE KEY (StationId,ControlAreaUnitId,ControlAreaId),
								FOREIGN KEY (StationId) REFERENCES Station(StationId),
								FOREIGN KEY (ControlAreaUnitId,ControlAreaId) REFERENCES ControlArea(ControlAreaUnitId,ControlAreaId)
							);	

CREATE TABLE ManagedBy (
					RemoteUnit VARCHAR(15) NOT NULL,
					ControlAreaUnitId INT NOT NULL,
					ControlAreaId VARCHAR(15) NOT NULL,
					UNIQUE KEY (RemoteUnit, ControlAreaUnitId, ControlAreaId),
					FOREIGN KEY (RemoteUnit) REFERENCES Remote(RemoteUnit),
					FOREIGN KEY (ControlAreaUnitId, ControlAreaId) REFERENCES ControlArea(ControlAreaUnitId, ControlAreaId)
				);
