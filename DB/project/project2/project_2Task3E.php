<html>
<head><title>MTA Database</title></head>
<body>
<?php
	include("DB_connect.php");

	//Tabulating station names, ids and total numbers of control areas in the middle 
	//town area, e.g., latitude between 40.750 and 40.760 and longitude between -74.000 and -73.950
	$sql = "SELECT s.StationName, tmp.StationId, tmp.ControlAreaCount FROM Station s JOIN ".
		"(SELECT StationId, count(ControlAreaId) as ControlAreaCount FROM DataCollectedBy GROUP BY StationId)tmp ".
		"WHERE s.StationId = tmp.StationId and ".
		"s.Latitude > 40.750 and s.Latitude < 40.760 and s.Longitude > -74.000 and s.Longitude < -73.950";
	echo "<b>".$sql.";</b><br/>";
	$stationName = mysql_query($sql, $conn);
	echo "<table border = '1'><tr><td>StationName</td><td>StationId</td><td>ControlAreaCount</td></tr>";
	
	if($stationName === FALSE) {
    die(mysql_error()); // TODO: better error handling
}

	while ($row = mysql_fetch_array($stationName, MYSQL_NUM) )
	{
		echo "<tr><td>".$row[0]."</td><td>".$row[1]."</td><td>".$row[2]."</td></tr>";
	}
	echo "</table>";	

	mysql_close($conn);
?>
</body></html>