<html>
<head><title>MTA Database</title></head>
<body>
<?php
	include("DB_connect.php");

	$os = "linux";
    if( strtoupper(substr(php_uname(), 0, 3)) == 'WIN'){
		$LF = "\r\n";
		$os = "win";
		echo $LF."OS Version is windows<br/><br/>".$LF;
	}
	else {
		$LF = "\n";
		echo $LF."OS Version is Unix<br/><br/>".$LF;
	}

	//drop all tables before recreate (drop relationship sets first then drop entity sets)
	mysql_query("DROP TABLE Connects");
	mysql_query("DROP TABLE DataCollectedBy");
	mysql_query("DROP TABLE ManagedBy");
	mysql_query("DROP TABLE Turnstile");
	mysql_query("DROP TABLE RemoteUnits");
	mysql_query("DROP TABLE ControlArea");
	mysql_query("DROP TABLE SubwayLines");
	mysql_query("DROP TABLE Station");
	
	//create remote units table
	$sql_remoteUnits_table = "CREATE TABLE RemoteUnits (RemoteUnit VARCHAR(15) NOT NULL,PRIMARY KEY (RemoteUnit) )";
	echo "<b>".$sql_remoteUnits_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_remoteUnits_table, $conn);
	if ($b) {
		echo "RemoteUnits table created successfully<br/><br/>".$LF;
	}
	
	//create control area table
	$sql_controlarea_table = "CREATE TABLE ControlArea (ControlAreaUnitId INT NOT NULL,".
														"ControlAreaId VARCHAR(15) NOT NULL,".
														"PRIMARY KEY (ControlAreaUnitId, ControlAreaId) )";
	echo "<b>".$sql_controlarea_table.";</b><br/><br/>";
	$b = mysql_query($sql_controlarea_table, $conn);
	if ($b) {
		echo "ControlArea table created successfully<br/><br/>".$LF;
	}
	
	//create subway lines table
	$sql_subwaylines_table = "CREATE TABLE SubwayLines (LineName VARCHAR(30) NOT NULL,PRIMARY KEY (LineName) )";
	echo "<b>".$sql_subwaylines_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_subwaylines_table, $conn);
	if ($b) {
		echo "SubwayLines table created successfully<br/><br/>".$LF;
	}
	
	//create station table
	$sql_station_table = "CREATE TABLE Station (StationId INT NOT NULL,StationName VARCHAR(50),".
                                                "Latitude FLOAT,Longitude FLOAT,PRIMARY KEY (StationId) )";
	echo "<b>".$sql_station_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_station_table, $conn);
	if ($b) {
		echo "Station table created successfully<br/><br/>".$LF;
	}
	
	//create turnstile table
	$sql_turnstile_table = "CREATE TABLE Turnstile (ControlAreaUnitId INT NOT NULL,".
													"ControlAreaId VARCHAR(15) NOT NULL,".
													"SCP VARCHAR(15) NOT NULL,".
													"UNIQUE KEY (ControlAreaUnitId, ControlAreaId, SCP),".
													"FOREIGN KEY (ControlAreaUnitId, ControlAreaId) ".
													"REFERENCES ControlArea(ControlAreaUnitId, ControlAreaId) )";
	echo "<b>".$sql_turnstile_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_turnstile_table, $conn);
	if ($b) {
		echo "Turnstile table created successfully<br/><br/>".$LF;
	}

	//create table connects
	$sql_connects_table = "CREATE TABLE Connects (StationId INT NOT NULL,LineName VARCHAR(30) NOT NULL,".
													"UNIQUE KEY (StationId, LineName),".
													"FOREIGN KEY (StationId) REFERENCES Station(StationId),".
													"FOREIGN KEY (LineName) REFERENCES SubwayLines(LineName) )";	
	echo "<b>".$sql_connects_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_connects_table, $conn);
	if ($b) {
		echo "Connects table created successfully<br/><br/>".$LF;
	}

	//create data collected by table
	$sql_datacollectedby_table = "CREATE TABLE DataCollectedBy (StationId INT NOT NULL,".
																"ControlAreaUnitId INT NOT NULL,".
																"ControlAreaId VARCHAR(15) NOT NULL,".
																"UNIQUE KEY (StationId,ControlAreaUnitId,ControlAreaId),".
																"FOREIGN KEY (StationId) REFERENCES Station(StationId),".
								                                "FOREIGN KEY (ControlAreaUnitId,ControlAreaId) ".
																"REFERENCES ControlArea(ControlAreaUnitId,ControlAreaId) ".
																")";	
	echo "<b>".$sql_datacollectedby_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_datacollectedby_table, $conn);
	if ($b) {
		echo "DataCollectedBy table created successfully<br/><br/>".$LF;
	}

	//create table managed by
	$sql_managedby_table = "CREATE TABLE ManagedBy (RemoteUnit VARCHAR(15) NOT NULL,".
													"ControlAreaUnitId INT NOT NULL,ControlAreaId VARCHAR(15) NOT NULL,".
													"UNIQUE KEY (RemoteUnit, ControlAreaUnitId, ControlAreaId),".
													"FOREIGN KEY (RemoteUnit) REFERENCES Remote(RemoteUnit),".
													"FOREIGN KEY (ControlAreaUnitId, ControlAreaId) ".
													"REFERENCES ControlArea(ControlAreaUnitId, ControlAreaId) )";
	echo "<b>".$sql_managedby_table.";</b><br/><br/>".$LF;
	$b = mysql_query($sql_managedby_table, $conn);
	if ($b) {
		echo "ManagedBy table created successfully<br/><br/>".$LF;
	}

	mysql_close($conn);	
?>	
</body></html>