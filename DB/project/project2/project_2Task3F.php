<html>
<head><title>MTA Database</title></head>
<body>
<?php
	include("DB_connect.php");

	//Tabulating station names, ids, total numbers of control areas and total numbers of turnstiles in the middle 
	//town area, e.g., latitude between 40.750 and 40.760 and longitude between -74.000 and -73.950. 
	$sql = "SELECT s.StationName, tmp.StationId, tmp.ControlAreaCount, tmp2.scpCount FROM Station s JOIN ".
		"(SELECT StationId, count(ControlAreaId) as ControlAreaCount FROM DataCollectedBy GROUP BY StationId) tmp ".
		"JOIN (SELECT s1.StationId,count(t.SCP) as scpCount FROM Turnstyle t JOIN DataCollectedBy d JOIN Station s1 ".
		"WHERE t.ControlAreaId = d.ControlAreaId and s1.StationId = d.StationId GROUP BY s1.StationId) tmp2 ".
		"WHERE s.StationId = tmp.StationId and s.StationId = tmp2.StationId and ".
		"s.Latitude > 40.750 and s.Latitude < 40.760 and s.Longitude > -74.000 and s.Longitude < -73.95";
	echo "<b>".$sql.";</b><br/>";
	$stationName = mysql_query($sql, $conn);
	echo "<table border = '1'><tr><td>StationName</td><td>StationId</td><td>ControlAreaCount</td><td>scpCount</td></tr>";
		
	if($stationName === FALSE) {
    die(mysql_error()); // TODO: better error handling
}

	while ($row = mysql_fetch_array($stationName, MYSQL_NUM) )
	{
		echo "<tr><td>".$row[0]."</td><td>".$row[1]."</td><td>".$row[2]."</td><td>".$row[3]."</td></tr>";
	}
	echo "</table>";	

	mysql_close($conn);
?>
</body></html>