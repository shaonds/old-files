<html>
<head><title>MTA Database</title></head>
<body>
<?php
	include("DB_connect.php");
	
	$os = "linux";
    if( strtoupper(substr(php_uname(), 0, 3)) == 'WIN'){
		$LF_sql = "\\r\\n";
		$LF = "\r\n";
		$os = "win";
		echo "OS Version is windows<br/><br/><br/><br/>".$LF;
	}
	else {
		$LF_sql = "\\n";
		$LF = "\n";
		echo "OS Version is Unix<br/><br/><br/><br/>".$LF;
	}
	
	//drop existing table before create again
	mysql_query("DROP TABLE ControlRemote", $conn);
	mysql_query("DROP TABLE ControlSCP", $conn);

	//create control remote table to temporary store control remote data
	$sql_controlremote_table = "CREATE TABLE ControlRemote (".
					"ControlAreaUnitId INT,".
					"ControlArea VARCHAR(15),".
					"RemoteUnit VARCHAR(15),".
					"StationId INT,".
					"LineName VARCHAR(30))";
	$db = mysql_query($sql_controlremote_table, $conn);
	if ($db) {
		echo "ControlRemote table created successfully<br/><br/>".$LF;
	}	
	$controlRemoteFile = "http://134.74.112.65/CSc336/controlremote.csv";
	$controlRemoteHandle = file($controlRemoteFile);
	$controlRemoteTargetFile = "./controlremote.csv";
	$controlRemoteTarget = fopen($controlRemoteTargetFile,"w");
	foreach ($controlRemoteHandle as $line_num => $line) 
	{
		fwrite($controlRemoteTarget, $line);
	}	
	if(	$os == "linux") {
		$shellOutput = shell_exec("dos2unix ". $controlRemoteTargetFile." >  /dev/null; ");
	}	
	$sql_LoadControlRemoteData = "LOAD DATA LOCAL INFILE '".$controlRemoteTargetFile."' 
								INTO TABLE ControlRemote FIELDS TERMINATED BY ',' LINES TERMINATED BY '".$LF_sql."'
								IGNORE 1 LINES";
	echo $sql_LoadControlRemoteData."<br/><br/>".$LF;	
	$db = mysql_query($sql_LoadControlRemoteData,$conn);
	if ($db) {
		echo "ControlRemote table loaded successfully<br/><br/><br/><br/>".$LF;
	}
	
	//create control scp table to temporary store control scp data
	$sql_controlscp_table = "CREATE TABLE ControlSCP (ControlAreaUnitId INT,SCP VARCHAR(15))";
	$db = mysql_query($sql_controlscp_table, $conn);
	if ($db) {
		echo "ControlSCP table created successfully<br/><br/>".$LF;
	}
	$controlScpFile = "http://134.74.112.65/CSc336/controlscp.csv";
	$controlScpHandle = file($controlScpFile);
	$controlScpTargetFile = "./controlscp.csv";
	$target = fopen($controlScpTargetFile,"w");
	foreach ($controlScpHandle as $line_num => $line) 
	{
		fwrite($target, $line);
	}	
	if(	$os == "linux") {
		$shellOutput = shell_exec("dos2unix ". $controlScpTargetFile." >  /dev/null; ");
	}	
	$sql_LoadControlScpData = "LOAD DATA LOCAL INFILE '".$controlScpTargetFile."'
							INTO TABLE ControlSCP FIELDS TERMINATED BY ',' LINES TERMINATED BY '".$LF_sql."' IGNORE 1 LINES";
	echo $sql_LoadControlScpData."<br/><br/>".$LF;	
	$db = mysql_query($sql_LoadControlScpData,$conn);
	if ($db) {
		echo "ControlSCP table loaded successfully<br/><br/><br/><br/>".$LF;
	}

	//load data into station table
	$stationFile = "http://134.74.112.65/CSc336/station.csv";
	$stationHandle = file($stationFile);
	$stationTargetFile = "./station.csv";
	$stationTarget = fopen($stationTargetFile,"w");
	foreach ($stationHandle as $line_num => $line) 
	{
		fwrite($stationTarget, $line);
	}	
	if(	$os == "linux") {
		$shellOutput = shell_exec("dos2unix ". $stationTargetFile." >  /dev/null; ");
	}	
	$sql_LoadStationData = "LOAD DATA LOCAL INFILE '".$stationTargetFile."' INTO TABLE Station FIELDS TERMINATED BY ',' ".
							"LINES TERMINATED BY '".$LF_sql."' IGNORE 1 LINES (StationId, StationName, @ignore, @ignore, Latitude, Longitude)";
	echo $sql_LoadStationData."<br/><br/>".$LF;	
	$db = mysql_query($sql_LoadStationData,$conn);
	if ($db) {
		echo "Station table loaded successfully<br/><br/>".$LF;
	}
	
	//insert data into remote table
	$sql_insert_remote = "INSERT INTO Remote(RemoteUnit) SELECT DISTINCT RemoteUnit FROM ControlRemote";
	echo $sql_insert_remote."<br/><br/>".$LF;
	$db = mysql_query($sql_insert_remote,$conn);
	if ($db) {
		echo "Remote data inserted successfully<br/><br/>".$LF;
	}
	
	//insert data into control area table
	$sql_insert_controlArea = "INSERT INTO ControlArea(ControlAreaUnitId,ControlAreaId) ".
							"SELECT ControlAreaUnitId,ControlArea FROM ControlRemote";
	echo $sql_insert_controlArea."<br/><br/>".$LF;
	$db = mysql_query($sql_insert_controlArea,$conn);
	if ($db) {
		echo "ControlArea data inserted successfully<br/><br/>".$LF;
	}

	//insert data into subway lines table
	$sql_insert_subwaylines = "INSERT INTO SubwayLines(LineName) SELECT DISTINCT LineName FROM ControlRemote";
	echo $sql_insert_subwaylines."<br/><br/>".$LF;
	$db = mysql_query($sql_insert_subwaylines,$conn);
	if ($db) {
		echo "SubwayLines data inserted successfully<br/><br/>".$LF;
	}
	
	//insert data into connects table
	$sql_insert_connects = "INSERT INTO Connects(StationId, LineName) SELECT DISTINCT StationId, LineName FROM ControlRemote";
	echo $sql_insert_connects."<br/><br/>".$LF;
	$db = mysql_query($sql_insert_connects,$conn);
	if ($db) {
		echo "Connects data inserted successfully<br/><br/>".$LF;
	}
	
	//insert data into managed by table
	$sql_insert_managedby = "INSERT INTO ManagedBy(RemoteUnit, ControlAreaUnitId, ControlAreaId) ".
							"SELECT RemoteUnit, ControlAreaUnitId, ControlArea FROM ControlRemote";
	echo $sql_insert_managedby."<br/><br/>".$LF;
	$db = mysql_query($sql_insert_managedby,$conn);
	if ($db) {
		echo "ManagedBy data inserted successfully<br/><br/>".$LF;
	}
	
	//insert data into data collected by table
	$sql_insert_datacollectedby = "INSERT INTO DataCollectedBy(StationId, ControlAreaUnitId, ControlAreaId) ".
								"SELECT DISTINCT StationId, ControlAreaUnitId, ControlArea FROM ControlRemote";
	echo $sql_insert_datacollectedby."<br/><br/>".$LF;
	$db = mysql_query($sql_insert_datacollectedby,$conn);
	if ($db) {
		echo "DataCollectedBy data inserted successfully<br/><br/>".$LF;
	}
	
	//insert data into turnstile table
	$sql_insert_turnstile =	"INSERT INTO Turnstile(ControlAreaUnitId, ControlAreaId, SCP) ".
							"SELECT r.ControlAreaUnitId, r.ControlAreaId, scp.SCP FROM ".
							"ControlArea r JOIN ControlSCP scp WHERE r.ControlAreaUnitId = scp.ControlAreaUnitId";
	echo $sql_insert_turnstile."<br/><br/>".$LF;
	$db = mysql_query($sql_insert_turnstile,$conn);
	if ($db) {
		echo "Turnstile data inserted successfully<br/><br/><br/><br/>".$LF;
	}
	
	//cleanup all temp tables
	$db = mysql_query("DROP TABLE ControlRemote", $conn);
	if ($db) {
		echo "Temp Table ControlRemote dropped successfully<br/><br/>".$LF;
	}
	$db = mysql_query("DROP TABLE ControlSCP", $conn);
	if ($db) {
		echo "Temp Table ControlSCP dropped successfully<br/><br/>".$LF;
	}

	mysql_close($conn);
?>

</body></html>